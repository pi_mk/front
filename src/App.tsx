import React, { useReducer } from 'react';
import './App.scss';

import { reducer, initialState } from './HOC/AppDataProvider/reducer';
import AppDataProvider from './HOC/AppDataProvider/AppDataProvider';
import LoginPage from './views/LoginPage';
import Layout from './views/Layout';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

const App = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div className="App">
      <AppDataProvider value={{ state, dispatch }}>
        <Router>
          <Switch>
            <Route exact path="/">
              <LoginPage />
            </Route>

            <Route path="/dashboard">
              <Layout />
            </Route>
          </Switch>

        </Router>
      </AppDataProvider>
    </div>
  )
}

export default App;
