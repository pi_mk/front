import React from 'react';

import TopBar from 'components/layout/TopBar';
import { 
  Switch, 
  Route } from 'react-router-dom';


const Layout: React.FC = () => {
  return (
    <div className="layout">
      <div className="layout__header">
        <TopBar />
      </div>

      <div className="layout__body">
        <Switch>
          <Route path="">

          </Route>
        </Switch>
      </div>
    </div>
  )
}

export default Layout;