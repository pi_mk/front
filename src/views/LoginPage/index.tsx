import React from 'react';

import LoginForm from '../../components/account/LoginForm'

const LoginPage: React.FC  = () => {
  return (
    <div className="loginPage">
      <div className="loginPage__wrapper">
        <LoginForm />
      </div>
    </div>  
  )
}

export default LoginPage