/* eslint-disable no-case-declarations */
export const initialState = {
}

type State = {

}

type Action = {
  type: string
}

export const reducer = (state: State, action: Action) => {
  switch (action.type) {
    default:
      console.log('ACTION NOT FOUND !', action.type)
      return {...state}
  }
}