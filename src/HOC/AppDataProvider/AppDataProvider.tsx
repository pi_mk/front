import React, { useEffect } from 'react'

import AppDataContext from 'context/AppDataContext';
import ReactNotification, { store } from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
// import API from '../../services/api';


interface AppDataInterface {
  children: React.ReactElement,
  value: any
}

interface NotificationMessage {
  msg: string,
  type: "success" | "info" | "danger" | "default" | "warning" | undefined
}

const AppDataProvider: React.FC<AppDataInterface> = ({children, value}) => {
  const { state, dispatch } = value

  const actions = {
    addNotification: (message: NotificationMessage) => {
      store.addNotification({
        message: message.msg,
        type: message.type,
        insert: 'top',
        container: 'top-right',
        animationIn: ['animated', 'fadeIn'],
        animationOut: ['animated', 'fadeOut'],
        dismiss: { duration: 5000 }
      });
    },     
  }

  const isUserLoggedIn = async () => {
  }


  useEffect(()=>{
    isUserLoggedIn()

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return(
    <div>
      <ReactNotification />

      <AppDataContext.Provider value={{state, dispatch, actions}}>
        {children}
      </AppDataContext.Provider>
    </div>
  )
}

export default AppDataProvider