import React from 'react';

interface InputTypes {
  value: string,
  name ?: string,
  error?: any,
  label: string,
  disabled?: boolean,
  placeholder?: string,
  type: string,
  required?: boolean,
  className?: string,
  onChange?: any,
  onKeyDown?: any
}

const Input: React.FC<InputTypes> = ({value, name, label, type, disabled, placeholder, className, error, onChange, onKeyDown}) => {

  const renderSwitch = () => (
    <>
      <div className={`checkbox ${error ? 'error' : ''}`}>
        <div className="switch tiny">
          <input hidden id={name} value={value} onChange={e => onChange(name, e.target.value || value === '' ? 1 : 0)} name={name} type={'checkbox'} placeholder={placeholder || ''}  className={`form-control ${className} ${error ? 'error' : ''}`}/>
          <label className="switch-paddle" htmlFor={name}>
            <span className="show-for-sr">Zaakceptuj</span>
            <span className="switch-active" aria-hidden="true">Tak</span>
            <span className="switch-inactive" aria-hidden="true">Nie</span>
          </label>
        </div>
        <div className="checkbox__text" dangerouslySetInnerHTML={{
            __html: label
          }} />         
      </div>
      { error && <span className="field-error">{error}</span> }        
    </>
  )

  const renderCheckbox = () => (
    <label className="c-input c-checkbox">
      <input value={value} onChange={e => onChange(name, e.target.value)} name={name} type={type} placeholder={placeholder || ''}  className={`form-control ${className} ${error ? 'error' : ''}`}/>
      <span className="c-indicator"></span>
      { label }
      { error && <span className="field-error">{error}</span> }        
    </label>
  )

  const renderTextarea = () => (
    <>
      { label && <div className="label" dangerouslySetInnerHTML={{__html: label}}></div> }
      <textarea  
        onChange={e => onChange(name, e.target.value)} name={name}  
        placeholder={placeholder || ''} 
        className={`form-control ${className} ${error ? 'error' : ''}`} 
        defaultValue={value}/>
      { error && <span className="field-error">{error}</span> }     
    </>
  )

  const renderStandardInput = () => (
    <>
      { label && <div className="label" dangerouslySetInnerHTML={{__html: label}}></div> }
      <input 
        name={name} 
        value={value} 
        disabled={disabled}
        type={type || 'text'} 
        placeholder={placeholder || ''} 
        onKeyDown={e => onKeyDown(e)} 
        onChange={e => onChange ? onChange(name, e.target.value) : null} 
        className={`form-control ${className} ${error ? 'error' : ''}`}/>
      { error && <span className="field-error">{error}</span> }    
    </>
  )

  const renderStandardSelect = () => (
    <>
      { label && <div className="label" dangerouslySetInnerHTML={{__html: label}}></div> }
        <select>

        </select>
      { error && <span className="field-error">{error}</span> }         
    </>
  )

  const renderFieldByType = (type: string) => {
    switch(type) {
      case 'checkbox':
        return renderCheckbox()
      case 'switch':
        return renderSwitch()
      case 'textarea':
        return renderTextarea()
      case 'select':
        return renderStandardSelect()
      default:
        return renderStandardInput()
    }
  }


  return (
    renderFieldByType(type)
  )
}


export default Input