import React, { useState } from 'react';

import Input from 'components/UI/Input';

const LoginForm: React.FC = () => {
  const [fields, setFields] = useState({
    'login': '',
    'password': ''
  });

  const [errors, setErrors] = useState({
    'login': false,
    'password': false    
  })

  const _handleFieldChange = (name: keyof typeof fields, value: string) => {
    const tmpFields = {...fields};
    tmpFields[name] = value;

    setFields({...tmpFields});
  }

  const _handleKeyDown = (event: any) => {
    if (event.key === 'Enter') {
      // handleSubmit()
    }
  }

  const renderSingleFieldRow = (field: keyof typeof fields, type:string, label: string) => {
    return (
      <div className="form__row">
        <Input 
          type={type}
          name={field}
          label={label}
          error={errors[field]}
          value={fields[field]}        
          onKeyDown={_handleKeyDown}
          onChange={_handleFieldChange}/>
      </div>
    )
  }

  return (
    <div className="LoginForm">
      <form className="form">
        { renderSingleFieldRow('login', 'text', 'Login') }
        { renderSingleFieldRow('password', 'password', 'Hasło') }

        <div className="form__row">
          <button className="btn btn--1">Zaloguj</button>
        </div>
      </form>
    </div>
  )
}

export default LoginForm