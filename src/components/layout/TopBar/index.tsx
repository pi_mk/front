import React from 'react';

const TopBar: React.FC = () => {
  return (
    <header className="topBar">
      <div className="topBar__wrapper">
        <div className="topBar__logo"></div>

        <div className="topBar__menu">
          <span className="topBar__option">Wyloguj</span>
        </div>
      </div>
    </header>
  )
}

export default TopBar;